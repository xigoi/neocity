import { createApp } from '/scripts/vue.esm-browser.prod.min.js'

Array.prototype.shuffle = ->
  for item, i in this
    j = i + Math.floor(Math.random() * (this.length - i))
    this[i] = this[j]
    this[j] = item

Array.prototype.splitChunks = (size) ->
  this.slice(i, i + size) for i in [0...this.length] by size

app = createApp
  data: ->
    input: ''
    grid: []
    gridSize: 3
    bingoGenerated: no
  computed:
    phrases: ->
      @input.split('\n').filter((phrase) => phrase)
    enoughPhrases: ->
      @phrases.length >= @gridSize * @gridSize
  methods:
    generate: ->
      phrases = @phrases
      phrases.shuffle()
      phrases = phrases.slice(0, @gridSize * @gridSize)
      @grid = ({text, crossed: no} for text from row for row from phrases.splitChunks(@gridSize))
      @bingoGenerated = yes
  mounted: ->
    params = new URLSearchParams(window.location.search)
    if params.has 'file'
      fetch(params.get('file'))
        .then (response) => response.text()
        .then (text) => @input = text
    if params.has 'size'
      @gridSize = Number(params.get('size'))

app.mount document.body
