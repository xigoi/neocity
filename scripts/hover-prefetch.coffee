prefetch = document.createElement 'link'
prefetch.rel = 'prefetch prerender'
document.head.appendChild prefetch
for link from document.querySelectorAll 'a:not([href^="http://"]):not([href^="https://"]):not([href^="mailto:"])'
  link.addEventListener 'mouseover', (event) =>
    prefetch.href = event.target.href
