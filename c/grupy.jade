extends ../templates/mathsessions.jade

block title
  | Grupy

block article
  <div class="entry-content"><h1 class="entry-title">Grupy</h1>
  <p>Tento článek je pokračováním k <a href="http://mathsessions.klusik.cz/2021/01/02/semigrupy/">článku o semigrupách</a>.</p>
  <h2>Definice</h2>
  <p>Už jsme si vysvětlili, co jsou to grupoidy, semigrupy a monoidy. Nyní povýšíme ještě o jednu úroveň a definujeme <em>grupu</em>.</p>
  <p class="has-light-blue-background-color has-background">Monoid \((G; \cdot)\) nazveme <em>grupou</em>, pokud v něm funguje <em>inverze</em>. To znamená, že ke každému prvku \(x \in G\) existuje nějaký inverzní prvek \(x^{-1} \in G\) (ne nutně různý od \(x\)), pro který platí \(x \cdot x^{-1} = x^{-1} \cdot x = e\) (kde \(e\) je prvek identity, který máme již zahrnutý v definici monoidu).</p>
  <h2>Příklady</h2>
  <p>Pojďme zjistit, zda náš oblíbený grupoid \((\mathbb Z; +)\) je grupa. Již jsme si dokázali, že jde o monoid, kde prvkem identity je \(0\), takže nyní nám stačí dokázat, že v něm všechny prvky mají inverzi.</p>
  <p>Vezměme si například \(x = 6\). Chceme-li najít inverzní prvek, řešíme rovnici \(6 + x^{-1} = x^{-1} + 6 = 0\). Řešením této rovnice je \(x^{-1} = -6\), a to je tedy inverzním prvkem ke zvolenému \(x\). Zároveň zřejmě podobným způsobem najdeme inverzi ke každému prvku \(x\), a sice \(-x\). Z toho plyne, že \((\mathbb Z; +)\) je grupa.</p>
  <p>Povšimněme si, že inverzí \(0\) je opět \(0\). To platí pro prvek identity v každé grupě, neboť řešením rovnice \(e \cdot e^{-1} = e\) je právě \(e^{-1} = e\).</p>
  <p>Jako druhý příklad zjistěme, zda \((\mathbb Z; \cdot)\) je grupa (kde \(\cdot\) tentokrát skutečně značí násobení). Dá se celkem snadno dokázat, že jde o monoid s identitou \(1\), takže nás opět zajímá hlavně inverze. Pokud si vybereme například \(x = -1\) a budeme k němu hledat inverzi, dostaneme se na rovnici \((-1) \cdot x^{-1} = 1\), která má řešení \(x^{-1} = -1\). Inverzi však musí mít všechny prvky, a pokud vybereme třeba \(x = -7\), rovnice \((-7) \cdot x^{-1} = 1\) už řešení v oboru celých čísel mít nebude. \((\mathbb Z; \cdot)\) tedy sice je monoid, ale grupa už ne.</p>
  <p>Pokračujme nyní s příklady S2 a S4 z článku o semigrupách. (O grupoidu v příkladu S3 jsme zjistili, že není monoid, tudíž nemůže být ani grupa.)</p>
  <h5>S2</h5>
  <p>Máme množinu tří funkcí \(F = \{f(x),g(x),h(x)\}\) definovaných takto:</p>
  <p>\[f(x) = x, \\ g(x) = 1-\frac{1}{x}, \\ h(x) = \frac {1}{1-x}. \\ D(f) = \mathbb{R} – \{0,1\}, \\ D(g) = \mathbb{R}-\{0\}, \\ D(h) = \mathbb{R}-\{1\}.\]</p>
  <p>Již jsme dokázali, že \((F, \circ)\), kde \(\circ\) značí skládání funkcí, tvoří monoid s identitou \(f\). Také jsme si sestavili přehlednou tabulku:</p>
  <p>\[\begin {array} {c|ccc} \circ &amp; f(x) &amp; g(x) &amp; h(x) \\ \hline f(x) &amp; f(x) &amp; g(x) &amp; h(x) \\ g(x) &amp; g(x) &amp; h(x) &amp; f(x) \\ h(x) &amp; h(x) &amp; f(x) &amp; g(x) \\ \end {array}\]</p>
  <p>Máme-li takovouto tabulku, potom je velmi snadné určit, zda se jedná o grupu. Chceme, aby každý prvek měl inverzi, což neznamená nic jiného, než že v příslušném řádku najdeme prvek identity. Inverzní prvek poté nalezneme v záhlaví odpovídajícího sloupce. Musíme si však dát pozor na to, že při obrácení pořadí prvků musí také vyjít identita (grupa obecně nemusí být komutativní) — to již snadno v tabulce dohledáme.</p>
  <p>V tomto konkrétním případě tedy zjistíme, že \(f^{-1} = f\), \(g^{-1} = h\) a \(h^{-1} = g\). Dokázali jsme, že \((F; \circ)\) je grupa.</p>
  <h5>S4</h5>
  <p>Zde máme algebraickou strukturu \(( (-c; c); \oplus)\) s nějakou konstantou \(c \in \mathbb R^+\) (v praxi jde o rychlost světla), kde operace \(\oplus\) je definována následovně:</p>
  <p>\[u \oplus v = \frac{u+v}{1+{\frac{uv}{c^2}}}\]</p>
  <p>Tu nejtěžší část máme již za sebou — dokázali jsme, že je to grupoid, semigrupa i monoid (s identitou \(0\)). Nyní máme daný nějaký prvek \(u\) a chceme k němu najít inverzi. To znamená, že řešíme rovnici:</p>
  <p>\[\frac{u+u^{-1}}{1+{\displaystyle\frac{uu^{-1}}{c^2}}} = 0\]</p>
  <p>Řešení je velmi jednoduché: obě strany rovnice vynásobíme jmenovatelem velkého zlomku. Ten nemůže být \(0\), protože jsme si již dokázali, že výraz je vždy definovaný. Dostaneme se tím na tvar \(u + u^{-1} = 0 \Longrightarrow u^{-1} = -u\). Tato inverze bude jistě ležet v nosné množině, neboť ta obsahuje vzájemně si odpovídající kladná a záporná čísla. Máme tedy dokázáno, že \(( (-c; c); \oplus)\) je grupa.</p>
  <h2>Věty</h2>
  <p>Nyní si dokažme pár zajímavých vět o grupách:</p>
  <p class="has-yellow-background-color has-background">Věta: Každý prvek grupy má právě jednu inverzi.</p>
  <p>Důkaz: Předpokládejme, že by jeden prvek \(x\) měl dvě různé inverze \(x^{-1}_1\) a \(x^{-1}_2\). Podle definice inverze můžeme psát:</p>
  <p>\[xx^{-1}_1 = e = xx^{-1}_2\]</p>
  <p>(Operaci grupy zde značíme jako násobení bez tečky.) Vynásobíme obě strany rovnice zleva \(x^{-1}_1\):</p>
  <p>\[x^{-1}_1(xx^{-1}_1) = x^{-1}_1(xx^{-1}_2)\]</p>
  <p>V grupě platí asociativita, takže můžeme přehodit závorky:</p>
  <p>\[(x^{-1}_1x)x^{-1}_1 = (x^{-1}_1x)x^{-1}_2\]</p>
  <p>Nyní můžeme opět uplatnit definici inverze:</p>
  <p>\[ex^{-1}_1 = ex^{-1}_2\]</p>
  <p>A nakonec použijeme definici identity:</p>
  <p>\[x^{-1}_1 = x^{-1}_2\]</p>
  <p>Tím jsme se dostali do sporu s předpokladem, že jsme vybrali dvě různé inverze. Věta tedy musí platit.</p>
  <p class="has-yellow-background-color has-background">Věta: Každá grupa je kvazigrupa, tedy funguje v ní krácení a dělení. (Více informací o kvazigrupách najdete v našem článku o nich.)</p>
  <p class="has-light-blue-background-color has-background">Definice krácení: Pokud pro nějaká \(x, y, z \in G\) platí \(xz = yz\) nebo \(zx = zy\), potom \(x = y\).</p>
  <p>Důkaz krácení: Začněme rovností \(xz = yz\). Obě strany vynásobíme zprava \(z^{-1}\) a máme \(xzz^{-1} = yzz^{-1}\). (Díky asociativitě není nutné zde psát závorky.) Poté podle identity máme \(x = y\). Podobně můžeme dokázat i druhou část, stačí násobit zleva místo zprava.</p>
  <p class="has-light-blue-background-color has-background">Definice dělení: Pro každé dva prvky \(x, y \in G\) existují dva prvky \(a, b \in G\) (ne nutně různé) takové, že \(ax = y = xb\).</p>
  <p>Důkaz dělení: Zvolíme \(a = yx^{-1}\) a \(b = x^{-1}y\). Poté máme \(ax = yx^{-1}x = y\) a \(xb = xx^{-1}y = y\). Tím je důkaz hotov.</p>
  <p>Tím zároveň víme, že každá grupa je lupa, neboť lupa je definována jako kvazigrupa s identitou a grupa samozřejmě má identitu.</p>
  <!--  .author-info  -->
  </div>
